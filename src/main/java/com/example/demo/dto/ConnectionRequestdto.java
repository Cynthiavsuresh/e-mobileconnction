package com.example.demo.dto;

import com.example.demo.entity.Mobiledetails;
import com.example.demo.entity.Talktimeplans;
import com.example.demo.entity.Userdetails;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConnectionRequestdto {
	
//	@OneToOne
//	@JoinColumn(name="mid")
	@NotBlank(message="mobile Id is mandatory.....")
	private Mobiledetails mobile;	
//	@OneToOne
//	@JoinColumn(name="planid")
	@NotBlank(message="plan Id is mandatory.......")
	private Talktimeplans plan;	
//	@OneToOne
//	@JoinColumn(name="aadharid")
	@NotBlank(message="aadhar Id is mandatory......")
	private Userdetails user;	
	@Email(message="Ivalid email structure")
	private String email;	

}
