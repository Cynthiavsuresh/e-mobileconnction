package com.example.demo.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Userdto {
	
	@NotBlank(message="Name feild is Mandatory...")
	private String name;	
	@Email(message="Ivalid email structure.....")
	private String email;	
	@Min(value=18, message ="user should be minimum 18 year old to Apply....")
	private int age;

}
