package com.example.demo.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Userdetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long aadharid;
	private String name;	
	private String email;		
	private int age;	


}
