package com.example.demo.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConnectionRequest {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String requestId;
	@OneToOne
	@JoinColumn(name="mid")
	private Mobiledetails mobile;	
	@OneToOne
	@JoinColumn(name="planid")
	private Talktimeplans plan;	
	@OneToOne
	@JoinColumn(name="aadharid")
	private Userdetails user;	
	private String email;		
	private String connectionstatus;	


}
