package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Mobiledetails;

public interface Mobilerepository extends JpaRepository<Mobiledetails, Long> {

}
