package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Talktimeplans;

public interface Talktimerepository extends JpaRepository<Talktimeplans, Long> {

}
