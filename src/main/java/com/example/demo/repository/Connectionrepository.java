package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.ConnectionRequest;

public interface Connectionrepository extends JpaRepository<ConnectionRequest, String> {

}
